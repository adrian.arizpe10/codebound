(function () {
"use strict";

//How to use jQuery to attach event listeners to HTML elements

    //Mouse Events
    /*
    .click() - event handler to the 'click'
    .dblclick() - event handler to the 'double click'
    .hover() - executed when the mouse pointer enters and leaves the element(s)

     */
//Mouse Events in JS
//     var eventElement = document.getElementById('#my-id');
//     eventElement.addEventListener('click', function (e) {
//         alert('My id was clicked');
//     })

//Mouse Events in jQuery
//.click()
//SYNTAX: $('selector').click(handler);

    // $('#bravo').click(function () {
    //     alert('H1 with the id of bravo was clicked');
    // })

    //dblclick
    // $('#bravo').dblclick(function () {
    //     alert('H1 with the id of bravo was double clicked');
    // })
        //*research .dblclick() vs .ondblclick()

    //.hover()
    //SYNTAX: $('selector').hover(handlerIn, handleOut)

    // $('#bravo').hover(
    //     //handlerIn anonymous function
    //     function () {
    //         $(this).css('background-color', 'blue');
    //         //handlerOut anonymous function
    // },
    //     function () {
    //         $(this).css('background-color', 'white');
    //     }
    //     );

        // $('.important').hover(
        //     //handlerIn anonymous function
        //     function () {
        //         $(this).css('background-color', 'red');
        //     },
        //     //handlerOut anonymous function
        //     function () {
        //         $(this).css('background-color', 'green');
        //     }
        // );


        //Keyboard Events in jQuery
    /*
    Different types of keyboard events:
    .keydown()
    .keypress()
    .keyup()
    .on()
    .off()
     */

    //.keydown() - event handler to the "keydown" Javascript event / trigger that event on an element
    //SYNTAX: $(selector).keydown(handler)

        // $('#my-form').keydown(function () {
        //     alert('Hey! You pushed down a key in the form');
        // });

    //.keypress() - same as keydown, with one exception: Modifier keys (shift, control, escape)
    //will trigger .keydown() but NOT .keypress()

    //.keyup()
    //SYNTAX
    // $('#my-form').keyup(function () {
    //     alert('Hey! You pushed down a key in the form');
    // });


    //.on() and .off()
    // $('#bravo').on('click', function () {
    //     console.log('clicked');
    //     alert('on event triggered');
    // });

    // $('#bravo').off('click');

})()