(function () {
"use strict";

$(document).ready(function () {
//jQuery Effects

    /*
    .hide() - hides the element(s)
    .show() - display the element(s)
    .toggle() - displays / hides the element(s)
     */

    //When you click on 'Individual Artists', hide the listing of artists
    // $('#music-artist-toggle').click(function () {
    //     $('#music-artist').hide();
    // })



    //.show()
    // $('#music-artist-toggle').click(function () {
    //     $('#music-artist').show();
    // });
    // $('#music-band-toggle').hover(
    //     function () {
    //     $('#music-bands').show();
    // }, function () {
    //     $('#music-bands').hide();
    //     });

    //.toggle()
    // $('#music-artist-toggle').click(function () {
    //     $('#music-artist').toggle();
    // });


    //Animated Effects
    //Fading Effects
    //.faceIn() .fadeOut() .fadeToggle()
    $('#music-artist-toggle').click(function () {
        $('#music-artist').fadeToggle(1000);
    });

    //Sliding Effects
    //.slideUp() .slideDown() .slideToggle
    $('#music-band-toggle').click(function () {
        $('#music-bands').slideDown(1000);
    })



});
})()