//Conditional Statements

//if statements - will run only once if it meets certain conditions - that condition will return true or false

//Basic Syntax

if (condition) {
    //execute this code if condition is met
}


//switch statements
//less duplicated and increases readability in code

//Basic Syntax
switch (condition) {
    case ''://code that gets executed :
        break;
    case '':
    break;
    case '':
    break;
    default: //equivalent to else statement
        break;
}

//example
var color = 'pink' ;
switch (color) {
    case 'blue':
    console.log('This is blue')
        break;
    case 'red':
        console.log('This is red')
        break;
    case "pink":
        console.log('This is pink')
        break;
    default:
        console.log('You chose the wrong color')

}

//Ternary Operators
//shorthand way of creating if / else statements
//only used when there are only two choices
//Basic Syntax

//(if this condition is true) ? run this code : otherwise run this code instead

//Example
var numberOfLives = 0;
(numberOfLives === 0) ? console.log('Game Over') : console.log('I\'m still alive')