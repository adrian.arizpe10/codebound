(function () {  //IIFE GOES FIRST
    "use strict" // USE STRICT GOES HERE

//Basic Syntax
// [] // an empty array
//['pink', 'purple', 'blue', 'black'] // each individual value is considered an element
    //['pink', 'purple', 'blue', 'black'] // 4 elements
//[4, 6, 8, 10, 12]
    //[2, 3, 4, 5, [2, 4, 6, 8]] // You can store arrays inside of arrays - Nested Arrays
// var colors = ['black', 'green', 'blue', 'purple', 'yellow', 'orange']
//     console.log(colors);
//     console.log(colors.length);
//
// //accessing arrays
// //zero-indexed
//     var codebound = ['stephen', 'faith', 'karen', 'leslie', 'twyla']
//     console.log(codebound[4])
//     console.log(codebound[3])
//     console.log(codebound[2])
//     console.log(codebound[1])
//     console.log(codebound[0])
//     console.log(codebound[5]) //undefined
//     console.log('There are ' + codebound.length + ' people on the Codebound team')
//
//     console.log(codebound)
//
//     //iterating array
//     //traverse through elements
//     var codebound = ['stephen', 'faith', 'karen', 'leslie', 'twyla']
//
//     //loop through an array and console.log the values
//     for (var i = 0; i < codebound.length;i+=1){
//         console.log('Here is someone on the Codebound team: ' + codebound[i])
//     }

    //forEach Loop\
    //Basic Syntax
    // array.forEach(function(element, index, array)) {
    //
    //}
    //Keep naming conventions to plural nouns
    //var numbers = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    // var colors = ['blue', 'black', 'purple', 'green']
    //
    // colors.forEach(function (colors) {
    // //do something
    //     console.log(color)
    // })
    //

    //Changing / Manipulating Arrays
    // var fruits = ['bananas', 'apples', 'grapes', 'strawberry']

    // //Adding to array
    // console.log(fruits)

    // //.push - allows us to push an element into our array
    // fruits.push('watermelon');
    // console.log(fruits)

    // //.unshift add to the beginning of an array
    // fruits.unshift('cherry')
    // console.log(fruits)
    //
    // fruits.push('mandarin', 'dragon fruit', 'starfruit');
    // console.log(fruits);
    //


    // //Remove / Delete from an array
    // //.pop - // Remove or delete last element in array
    // fruits.pop();
    // console.log(fruits);

    // //.shift // Removes the first element in an array
    // fruits.shift();
    // console.log(fruits);
    // var removedFruit = fruits.shift(); //storing in a variable
    // console.log('Here is the fruit I removed: ' + removedFruit);

    //Locate Array elements
    // var fruits = ['bananas', 'apples', 'grape', 'strawberry']
    // // .indexOf -- Returns only first occurrence of what you are looking for
    // var index = fruits.indexOf('grape')
    // fruits.indexOf('grape')
    // console.log(index)
    // // .lastIndexOf starts at the end of an array
    // index = fruits.lastIndexOf('grape')
    // console.log(index)

    //------Slicing
    //.slice - Copies a portion of the array
    //returns a new array
    // var fruits = ['bananas', 'apples', 'grape', 'strawberry', 'mandarin']
    //
    // var slice = fruits.slice(2,5);
    // console.log(slice);
    // slice = fruits.slice(1);
    // console.log(slice);

    //Reverse
    //.reverse
    //Will reverse the original array AND return the reversed
//    var fruits = ['banana', 'grape', 'apple', 'strawberry', 'mandarin', 'dragonfruit']

    // fruits.reverse();
    // console.log(fruits);

    //Sorting
    // .sort
    //will sort the original array AND return the sorted array
    // var fruits = ['banana', 'grape', 'apple', 'strawberry', 'mandarin', 'dragonfruit', 'starfruit']
    //
    // fruits.sort();
    // console.log(fruits);


    //Splitting
    //.split - takes a string and turns it into an array
    // var codebound  = 'karen, faith, steven';
    // console.log(codebound);
    // var codeBoundArray = codebound.split(',')
    // console.log(codeBoundArray);

    //Joining
    //.join - takes an array and converts it to a string with a delimiter of your choice
    var codeBoundArray = ['karen', 'faith', 'stephen']
    console.log(codeBoundArray);
    var newCodeBound = codeBoundArray.join(',');
    console.log(newCodeBound);






























})()
