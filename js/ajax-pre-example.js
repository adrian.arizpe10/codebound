(function () {
    "use strict";

var people = [
    {
        name: 'faith',
        favFood: "pizza",
    },
    {
        name: 'henry',
        favFood: "sushi",
    },
    {
        name: 'jonathan',
        favFood: "enchiladas",
    },
    {
        name: 'eric',
        favFood: "tacos",
    },
    {
        name: 'adrian',
        favFood: "acai bowl",
    },
    {
        name: 'maryAnn',
        favFood: "street tacos",
    },
    {
        name: 'sandra',
        favFood: "hamburgers",
    }
];
    function buildHTML(peopleObjects) {
        var totalHTML = '<ul>';
        //forEach
        // peopleObjects.forEach(function (person) {
        //     totalHTML += "<li>";
        //     totalHTML += person.name;
        //     totalHTML += '</li>'
        // });
        //For Loop
        for (var i = 0; i < peopleObjects.length; i+= 1) {
            totalHTML += "<li>";
            totalHTML += peopleObjects[i].favFood;
            totalHTML += '</li>'
        }

        totalHTML += "</ul>"
        return totalHTML;
    }

    $('#add-names').click(function () {
        $('#listOfNames').html(buildHTML(people));
    });






})()
