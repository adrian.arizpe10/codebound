(function () {
"use strict";

//NPM - Node Package Manager
//     NPM is an online repository for the publishing of open source Node.js projects

    // NPM - Open source package library in JS
//Used in high performance applications

//NPM - NODE PACKAGE MANAGER
//****Open package source library within javascript****
//When to use NPM
//The name npm (Node Package Manager) stems from when npm first was created as a package manager for Node.js.
//
// All npm packages are defined in files called package.json.
//
// The content of package.json must be written in JSON.
//
// At least two fields must be present in the definition file: name and version.
//
//BENEFITS
/*
-manage local dependencies of projects tools.
-manage globally installed project tools
-manage multiple versions of code and code dependencies
-download standalone tools you cn use right away
-NPM provides package-lock.json which displays all dependencies of the project
*/



})()