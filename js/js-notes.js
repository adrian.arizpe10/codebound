"use strict"//helps us create / write cleaner code
console.log("Hello from Console"); //Method used to test our work


//This is a comment
/*This is a
comment
for multi lines
 */

//data types
//primitive types
//numbers, strings, boolean, undefined, null...objects,

//Variables
//var, let, const

//numbers - 100, -10, 0, 0.123"

//strings - "adrian", "email@.com", "iruggaerv"

//boolean - true, false

//undefined - unassigned var have this value and type

//null - special word for nothing

//Operators*******
//built in operators
Math.cos(9)
Math.pow(4,2); //exponent

//basic Arithmetic operators
//follow same rule of order
/*+ (add);
- (subtract);
* multiply;
/ (divide);
% modulus;
*/

//Logical operators*****
//returns a boolean value when used with boolean values
//also used with non boolean values but will return non boolean values
//&& and
// || or
// ! not

//Comparison operators*****
//will return a logical value based on whether or not the comparison is true
//can be any date type
//
//== : check if the value is the same
//=== : check if the value and the data type are the same
//!= : check if the value is not the same
//!==: check if value and data type are not the same

//4 == 4 : true
// 4==="4": false
//4!= 5: true
//4 == "4": true
//4 == 5: false

//<, >, <=, >=

//Concatenation
const name= 'Adrian';
const age= 27

console.log(name);
console.log(age);
console.log('Hello my name is '+ name + ' and I am ' + age);

//Template String
console.log(`Hello, my name is ${name} and I am ${age}`);
//Get the length of a string
console.log(name.length);

const s = 'Hello World';
console.log(s.length);

//Get the string in a uppercase
console.log(s.toUpperCase());

//Get the string in lowercase
console.log(s.toLowerCase());

//Index
console.log(s.substring(0, 5));
console.log(s.substring(6,11));

//User interaction
//alert()
//alert('Warning');
//alert('Welcome to my website');

//confirm
var confirmed = confirm('Are you sure you want to continue?');
console.log(confirmed);

//Prompt
var userInput = prompt( 'Enter your name: ');
console.log('The user enter: ' + userInput);

//Functions
//Function- is a reusable block of code that performs a specified task

//Syntax for Function
/*function nameOfFTheFunction(argument + arguments) {
    code you want the function to do
}
 Call a function
 Call it by its name with ()
 nameOfTheFunction ();call the function

 var result = nameOfTheFunction();
 console.log(result)

 nameOfTheFunction; this will NOT run the function

*/

function increment(num1) {
    return num1 = 1;
}
// call the function
console.log(increment(4))
//NaN = not a number
//Function increment(4) {
//return 4+1; //5
//}

var five = increment(4);
console.log(five); //5

//Anonymous Functions
//Including a name for our function is actually optional
//To define a function, we can also create an anonymous function
//Function without a name, and store it in a variable

//Syntax for Anonymous Function
var increment = function (num1) {
    return num1 + 1;

}; // Needs semicolon for anonymous function
//Calling an anonymous function
var two = increment(1);
console.log(two); //2

//Arguments / Parameters
//argument = the value that a function is called with
//parameters = part of the function's definition

function sum(a, b) {
    return a + b;
}

console.log(sum(10,51));

//A function without a return value
function shout(message) {
    alert(message.toUpperCase() + '!!!');
}
var returnValue = shout('Hey Yall');
console.log(returnValue);

//Function with no parameters and no return value
function greeting() {
 alert('hey there!');
}
//function greeting(message) { //has parameter
//    return message;
//}

console.log(greeting());
console.log(greeting('Hey Codebound!')); //passing string is ignored

//Shorthand Assignment Operators
/*
 y + x = x;
 Operator Original      Shorthand
    +=      x + 1 = x;   x +=1;
    -=      x - 1 = x;   x -+1;
    *=      x * 1 = x;   x *=1;
    /=      x / 1 = x;   x /=1;
    %=      x % 1 = x;   x %=1;

//Unary Operators
+ (plus)
- (negative)

++(increment) pre, post
-- (decrement)pre, post

Increment
var x = 2;
++x; // pre-increment = 3
x++; // post-increment = 2

function doSomething(input) {
    return output
}

Function Scope -
Block (local) vs Global Variables - declared outside function
Block (local) Variables - declared inside function
var example = "Hello, My name is Adrian" // global variable
function example() {
var example = "Hello, My name is Adrian" // local variable
}

//Global Variables
var example = "Hello, My name is Adrian"
function ex(){
}
Better practice - try using only local variables
 */
