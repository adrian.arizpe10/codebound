(function () {
'use strict';

//The jQuery Object

//object used to find and create HTML elements from the DOM
//Document Ready
// window.onload = function () {
//     alert('The page has finished loading!');
// }
//
// $(document).ready(function () {
//     alert('The page has finished loading!');
// });

//in jQuery, we use the dollar sign '$', to reference the jQuery object
//     $ is an alias of jQuery

    //jQuery Selectors
    //ID selector   #id  selects a single element with the given id attribute
    //CLASS Selector    .class  selects all elements with the given class name
    //Element Selector  element ex h1 selects all elements with the given tag name
    //Multiple selector selector1, selector2, ... selects the combined results of all the specific
    //All selector  * selects all elements


    //SYNTAX for jQuery Selectors:
    //$('selector')

    //.html - returns the html contents of selected element(s)
    //similar to 'innerHTML' property

    //.css - allows us to change css properties for a element(s)
    //Similar to the 'style' property

    //ID Selector
    //Syntax for selecting an element by id:
    //$('#id-name');

    // var content = $('#codeBound').html();
    // alert(content);
    //alert($('#codebound').html());

    //Class Selector
    //SYNTAX for selecting a element with a class name:
    //$('.class-name');
    // $('.urgent').css('background-color', 'red'); //single property
    // $('.urgent').css('text-decoration', 'underline'); //single property
    // $('.urgent').css({'background-color': 'yellow', 'text-decoration': 'dotted'}); //multiple properties require ({})
    //
    // //Element Selector
    // //SYNTAX $('element-name')
    // $('p').css('font-size', '50px');

    //Multiple Selector
    //SYNTAX: $('selector1, selector2, ....')

    // $('.urgent, p').css('background-color', 'orange');

    //ALL Selector
    //SYNTAX: $('*')

    $('*').css('border', '4px dotted red');

})()