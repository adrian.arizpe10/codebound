(function () {
"use strict";

//ES6 (ECMASCRIPT 2015) is a version of JS

//Exponent Operator
    //JS old way..
    // Math.pow(2, 8);
    // console.log(Math.pow(2, 8)); //256
    //
    // //ES6
    // console.log(2 ** 8); //256


    // CONST keyword
    // - blocked scope variable(s)
    //
    // without a block scope
    // if (true) {
    //     var cohort = 'bravo';
    // }
    // console.log(cohort); //bravo


    //with block scope
    // if (true) {
    //     const cohort = 'bravo';
    // }
    // console.log(cohort);


    //FOR Loop with block scope
    // for (var i = 1; i < 10; i+=1){
    //     console.log(i); //counts 0-9
    // }

    //FOR Loop without block scope
    // for (var i = 1; i < 10; i+=1){
    // }
    // console.log(i); // displays 10

    //TEMPLATE Strings
    // const cohort = 'bravo';
    // console.log('Hello ' + cohort.toUpperCase() + '!');//Concatenation
    //
    // console.log(`Hello ${cohort.toUpperCase()} !`);

    //For ... Of
    //SYNTAX: for (const element of iterable){
    //
    // }

    // const x = [10, 20, 30];
    // for (const value of x) {
    //     console.log(value);
    // }

    //Arrow Functions
    //- shorthand function syntax, EXCEPT 'this' is bound lexically

    //SYNTAX: js
    // const sayHello = function (cohort) {
    //     return 'Hello from ' + cohort;
    // };

    //Arrow
    // const sayHello = (cohort) => 'Hello from ' + cohort;
    // const sayHello = cohort => 'Hello from ' + cohort;


    //Default function parameter values
    //old way
    // function greeting(cohort) {
    //     if (typeof cohort === "undefined") {
    //         cohort = 'world';
    //     }
    //     return console.log('Hello, ' + cohort);
    // }
    // greeting();
    // greeting('bravo');

    //New Way
    // const greeting = (cohort = 'World') => console.log(`Hello from ${cohort}`);

    //another new way
    // function greeting(cohort = 'world') {
    //     console.log('Hello from ' + cohort);
    // }
    //greeting('codebound');


    //Object property variable assignment shorthand
    //Object from JS
    // var person = {
    //     name: 'Bravo',
    //     Age: 2
    // },
    // const name = 'Bravo';
    // const age = 2;
    // const person = {
    //     name,
    //     age,
    //     height: '5\'5\"'
    // };

    //Object Destructuring
    //shorthand for creating variables from object properties

    //old way...
    // var person = {
    //     name: 'Bravo',
    //     age: 2
    // };
    // var name = person.name;
    // var age = person.age;

    //New Way
    // const person = {
    //     name: 'bravo',
    //     age: 2
    // };
    // const {name, age} = person;


    //old way..
    function personInfo(person) {
        var name = person.name;
        var age = person.age;
    }

    //New way
    function personInfo({name, age}) {
        console.log(name);
    }

    //Destructuring an array

    const myArray = [1, 2, 3, 4, 5];
    const [x, y, thirdIndex, bob, karen] = myArray;
    console.log(x); //get 1
    console.log(y); // get 2
    console.log(bob); //get 4































})()