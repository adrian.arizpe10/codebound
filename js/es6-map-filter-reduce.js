'use strict';

//MAP, FILTER, AND REDUCE
// - all functions that operate on collections (arrays)
// - all will NOT modify but will return a new copy of the array

// .map = transform each element in the collection
// .filter = filters our values
// .reduce = reduces a collection to a single value

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// var evens = [];
//
// var odds = [];
//
// for (var i = 0; i < numbers.length; i+=1) {
//     if (numbers[i] % 2 === 0) {
//         evens.push(numbers[i]);
//     }
// }
// for (var i = 0; i < numbers.length; i+=1) {
//     if (numbers[i] % 2 != 0) {
//         odds.push(numbers[i]);
//     }
// }
//
// console.log(evens);
// console.log(odds);


//FILTER

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// var evens = numbers.filter(function (n) {
//     return n % 2 === 0;
// })
// console.log(evens);

// var odds = numbers.filter(function (n) {
//     return n % 2 != 0;
// })
// console.log(odds);


//MAP
// var increment = numbers.map(function (num) {
//     return num + 1;
// });
// console.log(increment);


//EXAMPLES ABOVE IN ES6
// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// const evens = numbers.filter(n => n % 2 === 0);
// const odds = numbers.filter(n => n % 2 != 0);
// const increment = numbers.map(num => num + 1);


//REDUCE
//.reduce = used to reduce a collection to a single value.

const nums = [1, 2, 3, 4, 5];

const sum = nums.reduce((accumulation, currentNumber) => {
   return accumulation + currentNumber;
}, 10);

console.log(sum)

const officeSales = [
    {
        name: 'Jim Halpert',
        sales: 500
    },
    {
        name: 'Dwight Schrute',
        sales: 750
    },
    {
        name: 'Ryan Howard started the fire',
        sales: 150
    }
];

    const totalSales = officeSales.reduce((total, person) => {
            return total + person.sales;

        }, 0);
    console.log(totalSales);

















