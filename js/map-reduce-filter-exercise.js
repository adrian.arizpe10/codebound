"use strict";
// MAP FILTER REDUCE EXERCISE
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];


/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/

const moreThanFiveLanguages = developers.filter(developer => developer.languages.length >=5);

var developerLanguages = developers.filter(function (l) {
    if (l.languages.length >= 5) {
        return l;
    }
})
console.log(moreThanFiveLanguages);
console.log(developerLanguages);


/**Use .map to create an array of strings where each element is a developer's
 email address*/

var emails = developers.map(mail => mail.email);
console.log(emails);


/**Use reduce to get the total years of experience from the list of developers.
 - Once you get the total of years you can use the result to calculate the average.*/

const totalYears = developers.reduce((total, year) => {
    return total + year.yearExperience;
}, 0);
console.log(totalYears);
const average = totalYears / developers.length;
console.log(average);

/**Use reduce to get the longest email from the list.*/

const longestEmail = developers.reduce((a, b) =>{
    return a.email.length > b.email.length ? a : b;
}).email;
console.log(longestEmail);

const longEmail = developers.reduce((longest, developer)=> {
    if (developer.email.length > longest.length) {
        return developer.email;
    } else {
        return longest;
    }
}, '');
console.log(longEmail);

/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, faith, dwight*/

const totalStaff = developers.reduce((names, staff) => {
    return names + staff.name + ', ';
    // return `${names}${staff.name}, `;

}, 'CodeBound Staff: "');
console.log(totalStaff);


/** BONUS: use reduce to get the unique list of languages from the list
 of developers
 */

const listOfLangs = Array.from(developers.reduce((accumulator, developer) =>
{
    developer.languages.map(language => accumulator.add(language));
    return accumulator;
}, new Set()));
    console.log(listOfLangs);

