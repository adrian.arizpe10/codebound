(function () {
    'use strict';

//Objects
//Object is a grouping of data and functionality.
// var firstName = 'Stephen';
// var lastName = 'Gueda';
// var age = 30;
// var address = '100 N Main St';

//Data Items inside of an object = PROPERTIES
//Functions inside of an object = METHODS

//CUSTOM OBJECTS
//Prototypes allows existing objecys to be used as templates to create new objects.
//Object keyword = the starting point to make custom objects

    //New object instance
    // var car = new Object();
    // console.log(typeof car);
//object

    //The use of 'new Object()' calls the object CONSTRUCTOR to build a new INSTANCE of Object.

    //OBJECT LITERAL NOTATION
    //-curly braces {}

    // var car = {};
    // alert(typeof car);

    //setting up properties on a custom object

    // var car = {};
    // //dot notation
    // car.make = 'Toyota';
    //
    // //array notation
    // car['model'] = '4Runner';
    // console.log(car);

    //Another way/ most common way to assign properties
    // var car = {
    //     make: 'Toyota',
    //     model: '4Runner',
    //     color: 'Black',
    //     numberOfWheels: 4
    // };
    // console.log(car);
    //
    // //Dont do this
    // car['numberofDoors'] = 4;
    //
    // //Instead
    // car.numberofDoors = 5;
    // console.log(car);


    //Nested Values
    //allows us to create complex data structures
    // var cars = [
    //     {
    //         make: 'Toyota',
    //         model: '4Runner',
    //         color: 'Black',
    //         numberOfWheels: 4,
    //         features: ['Automatic Doors', 'Bluetooth Connection'],
    //         alarm: function () {
    //             alert('Sound the alarm!');
    //
    //         }
    //     },
    //     {
    //         make:"Honda",
    //         model:'Civic',
    //         color: 'Green',
    //         numberOfWheels: 4,
    //         features: ['Great Gas Mileage', 'AM/FM Radio'],
    //         owner: {
    //             name: 'John Doe',
    //             age: 35
    //         },
    //         alarm: function () {
    //         alert("No alarm...sorry");
    //         }
    //     }
    // ];
    // //console.log(cars[0].features[0]);
    // //console.log(cars[1].owner.age);
    //
    // //display features for both cars
    // // console.log(cars);
    //
    // // cars.forEach(function (car) {
    // //     car.features.forEach(function (feature) {
    // //     console.log(feature);
    // //     })
    // //
    // // })
    // // console.log(cars[1].alarm());
    // //
    // // function sayHello() {
    // //     console.log('Hello');
    // }
    //     //Call the functions name
    // sayHello();

    //The 'this' keyword

//'this' is simply a reference to the current object -
    //'this' can refer to a different object based on how a function is called

    // var car = {};
    // car.make = 'Ford';
    // car.model = 'Mustang';
    //
    // //create a method on the car object
    // car.describeCar = function () {
    // console.log("Car make and model is:" + this.make + ' ' + this.model);
    // };
    // //calling the object's method
    // car.describeCar();

    var car = {
        make: 'Ford',
        model: 'Mustang',
        describeCar: function () {
            console.log(`Car make and model is ${this.make} ${this.model}`);

        }
    };

    car.describeCar();














})()