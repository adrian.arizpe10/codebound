(function () {
"use strict";

    //BOM Lecture Notes
    //Browser Object Model
    //hierarchy of objects in the browser
    //we can target and manipulate HTML elements with JS

        //Location
            //Location Object//
            //Manipulate the location of a document
            //get query string parameters
        //Navigator
            //Navigator Object//
            //query the info and capabilities of the browser
        //Screen
            //Screen Object//
            //get information about the screen
        //History
            //History Object//
            //get info / manage the web browsers screen
        //Window
        //Document - DOM

    //-----Window Object
    // window object represents the JS global object
    // all variables and functions declared globally with the var keyword become the properties of the window object

    // alert()
    // confirm()
    // prompt()
    // setTimeout() //allows us to set a time and execute a callback function once the timer expires
    // setInterval() //execute a callback function repeatedly with a fixed delay between each call

    //---setTimeout()

    //Basic Syntax
    //var timeoutID = setInterval(callbackFunction, [delay], arg1, arg2.......);
    //by default delay is set to 0
    //Delay is in Milliseconds

    // function timeoutEx() {
    //     setTimeout(function () {
    // alert('Hello Bravo Cohort and welcome to CodeBound')
    //     }, 2000);
    // }
    // timeoutEx()

    //setInterval()

    //Basic Syntax
    // var intervalID = setInterval(callbackFunction, delay, [arg1, arg2,...])
    // callbackFunction is executed every delay in milliseconds
    //delay is the time that the timer should delay between executions of the callbackFunction

    //clearInternal() //allows us to stop
    // clearInterval(intervalID);


    // var intervalID = setInterval(function () {
    // alert('Hi Faith')
    // },3000)
    //
    // clearInterval(intervalID);

    // var count = 0;
    // var max = 10;
    // var interval = 2000;
    //
    // var intervalID = setInterval(function () {
    //     if (count >= max) {
    //         clearInterval(intervalID);
    //         console.log('Finished')
    //     } else {
    //         count++
    //         console.log(count)
    //     }
    // }, interval);

    //Document Object Model
    //Manipulate HTML using JS
    //DOM is child of BOM
    //Locating elements
    // target by:
        //Element
        //Class
        //ID
    //getElementBy//
    //Basic Syntax
    // document.getElementsByName('Name of Element/Class/ID');

    //Example
    // var btnClicked = document.getElementById('btn3');
    // console.log(btnClicked);
    //
    // // Accessing form Inputs
    // //Access forms using the form collections.
    // var usernameInput = document.forms
    // // to collect specific
    // var usernameInput = document.forms.feedback-form;
    // console.log(usernameInput);
    //
    // //accessing HTML elements//
    // var cards = document.getElementsByTagName('section');
    // console.log(sections);
    //
    // //querySelector()
    // //returns the first element within that document that matches the specified selector or group of selectors.
    //
    // //var headerTitle = document.querySelector('header h1');
    // var headerTitle = document.querySelector('#main-title');
    // console.log(headerTitle);

    //querySelectorAll();

    // var cards = document.querySelectorAll('.card');
    // console.log(cards);



    //Accessing forms

    // var feedbackForm = document.forms['login'];

    // //same as

    // var feedbackForm = document.forms.login.username;

    // console.log(feedbackForm);


    //========Accessing and modifying Elements and properties

    //get value of innerHTML
    //         var title = document.getElementById('main-title');
    //         console.log(title.innerHTML);
    //
    //         console.log(document.getElementById('main-title').innerHTML);

    //set value of innerHTML

    // title.innerHTML = 'Hi <em>Stephen</em>!'


    //set value of innerText

    // var title = document.getElementById('main-title');
    // console.log(title.innerText);
    //
    // title.innerText = 'Hi Stephen!'
    //
    //
    // // append value to innerText(works the same with innerHTML)
    //
    // title.innerHTML += 'Hi Stephen!'


    //-------Accessing and modify using attributes

    // check is attributes exist

    var forms = document.forms['login'];
    console.log(form);

    console.log(form.hasAttribute('action'))


    //get an attribute value
     console.log(form.getAttribute('method'))

    //create a new attr or change a value of an existing attr

    form.setAttribute('id', 'feedback-form');

    form.setAttribute('method', 'GET');

    //Delete attr
    form.removeAttribute('action');

    console.log(form);

    // Accessing and modifying styles

    //single style

    var jumbotron = document.querySelector('.jumbotron');
    jumbotron.style.display = 'none';

    jumbotron.style.fontFamily = 'Comic Sans Ms'


    //multiple styles

   Object.assign(jumbotron.style,{
             border: '10px solid black',
             fontFamily: 'Trajan',
            textDecoration: 'underline',
    })   ;


          //Styling Node list

            var tableRows = document.getElementsByTagName('tr');
            console.log(tableRows);
            // for (var i = 0; i < tableRows.length; i += 1) {
            //     tableRows[i].style.background = 'hotpink';
            // }
            //
            // tableRows.forEach(function () {     //.WILL NOT WORK ON HTML COLLECTIONS (foreach is for arrays)
            //             tableRows.style.background = 'lightblue'
            // });
            //


            var tableRowArray = Array.from(tableRows);     //convert to array

            tableRowArray.forEach(function () {
                       tableRows.style.background = 'blue'
            })



            //adding and removing elements

            //createElement()
            //removeChild()
            //appendChild()
            //replaceChild()



















})()