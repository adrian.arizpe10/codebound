(function () {
"use strict";




//Event Listeners
    //addEventListener()


    // Basic Sytnax
    // target.addEventListener(type, listener[,useCapture]);
    //target = is the object the even listener is registered on
    //Type = is the type of even that is being listened for
    //Listener = is the function that is called when an event of type happens on the target
    //UseCapture = is a boolean that decides if the event-capturing is used for event triggering


    //Type of Events
    // 1. keyup (key is released)
    // 2. keydown (key is pressed down)
    // 3. Click (mouse is clicked)
    // 4. change (input gets modified)
    // 5. Submit (when form is submitted)


    // var testBtn = document.getElementById('login')
    // console.log(testBtn);
    // //add event listener using an anonymous function
    //
    // testBtn.addEventListener("click", function () {
    //     if (document.body.style.background === 'red') {
    //         document.body.style.background = 'white'
    //     } else {
    //         document.body.style.background = 'red'
    //     }
    //     console.log('test');
    // });

    //add event listener from a previously defined function

    // function toggleBackgroundColor() {
    //     if (document.body.style.background === 'red') {
    //         document.body.style.background = 'hotpink';
    //         testBtn.removeEventListener('click', toggleBackgroundColor)
    //     } else {
    //         document.body.style.background = 'red'
    //     }
    // }
    //
    // testBtn.addEventListener('click', toggleBackgroundColor)
    //
    //     //Remove event listener
    //
    // testBtn.removeEventListener('click', toggleBackgroundColor)
    //
    // //Register Additional Events
    //
    // //cursor hovers over a paragraph, change the color of the text, font-family, and make font larger
    //
    // var paragraph = document.getElementsByTagName('p')[0];
    //
    // //change font-size of whatever 'this' refers to
    // function makeColorChange() {
    //     paragraph.style.color = 'red';
    //     paragraph.style.fontFamily = 'Comic Sans MS';
    //     paragraph.style.fontSize = '30px';
    // }
    //
    // //adding a mouseover event to the p that fires off makeColorChange function
    // paragraph.addEventListener('mouseover', makeColorChange);
    //
    // //when double clicking the shrink text button, make the font size 1em.
    //
    // //element reference to shrink btn
    //
    // var shrinkBtn = document.getElementById('.shrinkBtn');
    //
    // shrinkBtn.addEventListener('dblclick', function () {
    //     paragraph.style.fontSize = '1em';
    // })

    //-------------Event Objects

    //Event Object
    // event.target
    //e

    // document.addEventListener('click', function (e) {
    //     var red = e.screenX % 256;
    //     var green = e.screenY % 256;
    //     var blue = 'ff'
    //
    //     console.log(e.screenX);
    //     console.log(e.screenY);
    //
    //     red = red.toString(16);
    //     green = green.toString(16);
    //     blue = blue.toString(16);
    //     document.body.style['background-color'] = '#' + red + green + blue;
    // })
    //
    // //Button
    // var button = document.getElementById('btn');
    // button.addEventListener('mousedown', function (e) {
    //     if (e.button == 0) {
    //         console.log('left button');
    //     } else if (e.button == 1) {
    //         console.log('middle button')
    //     } else if (e.button == 2) {
    //         console.log('right button')
    //     }
    // })


    //add click event to login btn that fires a function called logUserIn

    var loginBtn = document.getElementById('login');
    loginBtn.addEventListener('click', logUserIn)
    function logUserIn() {
        //console.log('Function was fired!');
        alert('Welcome to my site! Please log in')
        var age = prompt('You must be older than 18 to enter. Please enter your age: ');
        if (age <= 18){
            var answer = confirm('You are being redirected to a really weird site, do you want to continue?');
            if (answer) {
                window.location.replace("https://www.google.com")
            } else {
                alert('You made the right choice')
            }
        } else if ( age === null) {
            alert('Well you can\'t enter if you do not tell me your age!');
        }
    }















})()