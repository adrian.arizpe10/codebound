(function () {
"use strict";

$(document).ready(function () {

    //Attribute methods
    /*
    .html()
    .css()
    .addClass() - adds the specified class(es) to each of the set of matched elements
    .removeClass() - removed a single class, multiple classes or all classes
    .toggleClass() - add or remove one or more classes
     */

    //Getters / Setters

    //Method Chaining

    // $('#codebound').click(function () {
    //     $(this).html('CodeBound Rocks!');
    // });
    //
    // $('#name').css('color', 'firebrick').css('background-color', 'yellow');

    //.addClass()
    //SYNTAX: .addClass(className);

    // $('.important').click(function () {
    //     $(this).addClass('highlighted');
    // });
    //
    // //.removeClass()
    // $('.important').dblclick(function () {
    //     $(this).removeClass('highlighted');
    // });

    //.toggleClass()
    $('.important').click(function () {
        $(this).toggleClass('highlighted');
    });


});
})()