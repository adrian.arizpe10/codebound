(function () {
"use strict";

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS

    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

    // const waited = (delay) => {
    //     return new Promise((resolve, reject) => {
    //         setTimeout(() => {
    //             resolve(`You will see this after ${delay / 1000 } seconds`);
    //         }, delay);
    //     });
    // };
    // waited(4000).then((message) => console.log(message));

//Exercise :
//Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.

    // const toTen = (num) => new Promise ((resolve, reject) => {
    //     if (num > 10) {
    //         resolve('greater than ten')
    //     } else {
    //         reject('less than ten')
    //     }
    // })
    //
    // toTen(11)
    //     .then(function(value) {
    //         console.log(value)
    //     })
    //     .catch(function(error) {
    //         console.log(error)
    //     })
    //
    // const compareToTen = (num) => {
    //     const p = new Promise((resolve, reject) => {
    //         if (num > 10) {
    //             resolve(num + ' greater than ten')
    //         } else {
    //             reject(num + ' is less than ten')
    //         }
    //     })
    //     return p;
    // }
    //
    // compareToTen(0)
    //     .then(result => console.log(result))


//Exercise 3:
    //Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.


    const arrayWord = ['cucumber', 'tomatoes', 'avacado'];
    const complicatedArray = ['cucumber', 42, true];

    const makeAllCaps = (array) => {
        return new Promise((resolve, reject) => {
            var capsArray = array.map(word => {
                if (typeof word === 'string') {
                    return word.toUpperCase();
                } else {
                    reject('this promise has failed');
                }
            });
            resolve(capsArray);
        })
    }

    const sortWords = (array) => {
        return new Promise((resolve, reject) => {
            if (array) {
                array.forEach((el) => {
                    if (typeof el !== 'string') {
                        reject('Error: Not all values in array are strings');
                    }
                })
                resolve(array.sort())
            } else {
                reject("Something went wrong");
            }
        })
    }

    // makeAllCaps(arrayWord)
    //     .then(sortWords)
    //     .then((result) => console.log(result))
    //     .catch(error => console.log(error))

    makeAllCaps(complicatedArray)
        .then(sortWords)
        .then((result) => console.log(result))
        .catch(error => console.log(error))


    // const bikes = ['Trek', 'Giant', 'Scotty']
    //     const makeAllCaps = (word) =>
    //     new Promise((resolve, reject) => {
    //         if (word.every(word => typeof word === 'string')){
    //             resolve(word.map(word => word.toUpperCase()))
    //         } else {
    //             reject(Error('Promise failed'))
    //         }
    //     })
    //
    //
    // const sortWords = (words) => {
    //     return words.sort((a, b) => {
    //         if (a > b){
    //             return 1
    //         } else {
    //             return -1
    //         }
    //     })
    // }
    //
    // makeAllCaps(bikes)
    //     .then(words => sortWords(words))
    //     .then(result => console.log(result))
    //     .catch(error => console.log(error))



})()