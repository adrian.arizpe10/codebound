"use strict";

var x = new XMLHttpRequest(); //Opens HTTP REQUEST
x.open('GET','https://swapi.dev/api/planets/1/'); //URL can be file path / address
x.onreadystatechange =
    function () {
        if(x.readyState ===4){
            document.getElementById('content').innerHTML = x.responseText; //responseTxt - returns the text received from a server following a request being sent.
        }
    };
function sendTheAPI() {
    x.send();
    document.getElementById('reveal').style.display = 'none';
}

