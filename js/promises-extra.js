(function () {
"use strict";

const p = new Promise(((resolve, reject) => {
    const a = 1 + 1;
    if (a == 3){
        resolve('success')
    }else {
        reject('failed')
    };
}))

    p.then((message) => {
        console.log('This is in the then ' + message);
    }) .catch((message) => {
        console.log('This is in the catch' + message);
    })

let promise = new Promise((resolve, reject) =>  {
    setTimeout(() =>{
        reject('oops!')
    }, 1000);
});


    // const recordVideoOne = new Promise((resolve, reject) => {
    //     resolve('Video 1 Recorded')
    // });
    // const recordVideoTwo = new Promise((resolve, reject) => {
    //     resolve('Video 2 Recorded')
    // });
    // const recordVideoThree = new Promise((resolve, reject) => {
    //     resolve('Video 3 Recorded')
    // });
    //
    // Promise.all([
    //     recordVideoOne,
    //     recordVideoTwo,
    //     recordVideoThree
    // ]).then((messages) => {
    //     console.log(messages);
    // });


    const posts = [
        {title: 'Post One', body: 'This is post one'},
        {title: 'Post Two', body: 'This is post two'},

    ];
    function getPosts() {
        setTimeout(() => {
            let output = '';
            posts.forEach((post, index) => {
                output +=`<li>${posts}</li>`;
            });
            document.body.innerHTML = output;
        },1000);
    }
getPosts();

    function createPost(post, callback) {
        setTimeout(() =>{
            posts.push(post);
            callback;
        }, 2000)
    }

    createPost({title: 'Post Three', body: 'This is post three'}, getPosts);

    // function hasSumK(arr, k) {
    //     hashMap = {};
    //     for (let value of arr) {
    //         if (hashMap[value]) { return true;} else { hashMap[k - value] = true };
    //     }
    //     return false;
    // }
})()