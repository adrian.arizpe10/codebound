(function () {
"use strict";

    const developers = [
        {
            name: 'stephen',
            email: 'stephen@appddictionstudio.com',
            languages: ['html', 'css', 'javascript', 'php', 'java']
        },
        {
            name: 'karen',
            email: 'karen@appddictionstudio.com',
            languages: ['java', 'javascript', 'angular', 'spring boot']
        },
        {
            name: 'juan',
            email: 'juan@appddictionstudio.com',
            languages: ['java', 'aws', 'php']
        },
        {
            name: 'faith',
            email: 'faith@codebound.com',
            languages: ['javascript', 'java', 'sql']
        },
        {
            name: 'dwight',
            email: 'dwight@codebound.com',
            languages: ['html', 'angular', 'javascript', 'sql']
        },
        {
            name: 'adrian',
            email: 'adrian.arizpe10@gmail.com',
            languages: ['html', 'css', 'javascript', 'c++']
        }
    ];

        const name = 'adrian';
        const email = 'adrian.arizpe10@gmail.com';
        const languages = ['html', 'css', 'javascript', 'c++'];

    developers.push({
        name,
        email,
        languages
    });
        // const person = {
        //     name,
        //     email,
        //     languages
        // };
        // console.log(person);


    //
    // let emails = [];
    // let names = [];

    // TODO rewrite the following using arrow functions
    // developers.forEach(function(developer) {
    //     return emails.push(developer.email);
    // });
    // developers.forEach(developer => emails.push(developer.email)) ;

    // users.forEach(user => emails.push(user.email));
    // console.log(emails);

    // developers.forEach(function(developer) {
    //     return names.push(developer.name);
    // });
    // developers.forEach(developer => names.push(developer.name));

    // developers.forEach(function (developer) {
    //     return languages.push(developer.languages);
    // });
    //
    //  developers.forEach(developer => languages.push(developer.languages));
    //
    // const developers = [];


  // TODO: rewrite the code below to use object destructuring assignment
  //       note that you can also use destructuring assignment in the function
  //       parameter definition

// const name = developer.name;
// const email = developer.email;
// const languages = developer.languages;
//
// const {name, email, languages} = developer;

  // TODO: rewrite the assignment below to use template strings

//   developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
//
// });
// console.log(developers);

// developers.push(`${name} \'s email is ${email} ${name} knows ${languages.join(', ')}`);
// console.log(developers);

// TODO: Use `const` for the following variable
// const list = '<ul>';


// TODO: rewrite the following loop to use a for..of loop
// developers.forEach(function (developer) {

// for (const developer of developers) {
//     list += '<li>' + developer + '</li>';
// }

  // TODO: rewrite the assignment below to use template strings

//   list += '<li>' + developer + '</li>';
//
//     list += `<li> ${developer} </li>`;
// });
// list += '</ul>';
// document.write(list);

    const petName = 'Chula';
    const age = 14;
// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
    console.log("My dog is named " + petName + ' and she is ' + age + ' years old.');
    console.log(`My dog is named ${petName} and she is ${age} years old.`);

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
//     function addTen(num1) {
//         return num1 + 10;
//     }
//     const addTen = num1 => num1 + 10;

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
//     function minusFive(num1) {
//         return num1 - 5;
//     }
//     const minusFive = num1 => num1 -5;


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
//     function multiplyByTwo(num1) {
//         return num1 * 2;
//     }

    // const multiplyByTwo = num1 => num1 * 2;

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
//     const greetings = function (name) {
//         return "Hello, " + name + ' how are you?';
//     };
//
//     const greeting = name => "Hello, " + name + ' how are you?';

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
//     function haveWeMet(name) {
//         if (name === 'Bob') {
//             return name + "Nice to see you again";
//         } else {
//             return "Nice to meet you"
//         }
//     }
//I used multiple JavaScript ternary operators -adrian
//     const haveWeMet = name => (name === 'Bob') ? 'Nice to see you again' : ('Nice to meet you');

    //Ternary Operator
    //SYNTAX
    //Condition ? message a : message b

})()