"use strict";

//Loops

//while loop, for loop, break and continue

//while loop
//is a basic looping construct that will execute a block or code
//as long as a certain condition is true

//syntax
//while(condition) {
//  run code
//}

var n = 1;
while(n<=10) {
    console.log('#' + n);
    n++;
}

//Do while
//the only difference from a while loop is that the condition is evaluated at the end of the loop instead of the beginning

//syntax
//do {
    //run the code
//} while (condition)

var i = 10;
do {
    console.log('Do-while #' + i);
    i++;
} while (i <= 20);

//For Loops

//A for loop is a robust looping mechanism available in many programming languages

//syntax

//for (initialization; condition; increment/decrement) {
    //run code

for (var x = 0; x < 10; x++) {
    console.log('For Loop #' + x);
}

// Break and continue
// - breaking out of a loop
//using the 'break' keyword, allos us to exit the loop

var endAtNumber = 5;

for (var i = 1; i < 100; i++) {
    console.log('Loop count # ' + i);
    if (i === endAtNumber) {
        alert( 'We have reached the stopping point! Break!');
        break;
        console.log('Do you see this message?');
    }
}
//Continue the next iteration of a loop
//by using the 'continue' keyword

for (var n = 1; n < 100; n++) {
    if (n % 2 !== 0) {
        continue ;
    }
}
console.log('Even number: ' + n);