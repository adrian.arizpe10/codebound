(function () {
"use strict";

//PROMISESE ARE JUST JAVASCRIPT OBJECTS - CREATE CONST

//SYNTAX
//     fetch('http://localhost')
//         .then(response => console.log(response))
//         .catch(error => console.log(error));
//
//     const myPromise = fetch('http://');
//     myPromise.then(response => console.log(response));
//     myPromise.catch(error => console.log(error));

    // Promise.all accepts an array of promises, and resolves when all the individual promises have resolves
    // Promise.race: accepts an array of promises, and resolves when the first promise resolves

    //Chaining Promises together - the return value from a promises callback can itself be treated as a promise,
    // which allows us to chain promises together. Lets look at an example

    // Promise.resolve to immediately return a resolved promise

// We can create a promise objects like so


    // const myPromise = new Promise(((resolve, reject) => {
    //     if (Math.random() > 0.5) {
    //         resolve();
    //     } else {
    //         reject('You have an error');
    //     }
    // }));
    // myPromise.then(() => console.log('resolved'));
    // myPromise.catch(()=> console.log('rejected'));

    //Discovering Pendings

    // const myPromise = new Promise(((resolve, reject) => {
    //     setTimeout(() => {
    //         if (Math.random() > 0.5) {
    //             resolve();
    //         } else {
    //             reject();
    //         }
    //     }, setTimeout(1500))
    // }));
    // console.log(myPromise); //A pending Promise
    // myPromise.then(() => console.log('resolved'));
    // myPromise.catch(()=> console.log('rejected'));

    //Write a promise that simulates an api request

    // function makeRequest() {
    //     return new Promise((resolve, reject) => {
    //         setTimeout(() => {
    //             if (Math.random() > 0.1) {
    //                 resolve('Here is your data');
    //             } else {
    //                 reject('Network connection error');
    //             }
    //         }, 1500);
    //     });
    // }
    //
    // const request =  makeRequest();
    // console.log(request);
    // request.then(message => console.log('Promise resolved', message));
    // request.catch(message => console.log('Promises Rejected', message));

    const data = {username: 'example'};
 // //Dive into post vs put***************************************
    fetch('data/starWars.json', {
        method: 'Post',
        headers: {
            'Content-Type':'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .then(data => {
            console.log('success', data)
        })
        .catch((error) => {
            console.log('Error fetch gone bad', error);
        })

    // const isMomHappy = true;
    // //Promise
    // const willGetNewPhone = new Promise(((resolve, reject) => {
    //     if (isMomHappy) {
    //         const phone = {
    //             brand: 'Samsung',
    //             color: 'Black'
    //         };
    //         resolve(phone);
    //     } else {
    //         const reason = new Error('Mom is not happy');
    //         reject(reason);
    //     }
    // }))
    // //2nd promise
    // const showOff = function (phone) {
    //     const message = 'Hey friend, I have a new ' + phone.brand + ' ' + phone.color + ' phone';
    //     return Promise.resolve(message);
    // }
    //
    // //call our promise
    // const askMom = function () {
    //     willGetNewPhone
    //         .then(showOff)
    //         .then(fulfilled => console.log(fulfilled)
    //         )
    //         .catch(error => console.log(error.message))
    // }
    //
    // askMom();


    //.finally() - runs regardless if the promise resolves or is rejected

})()
