(function () {
"use strict";


    $('#hide').click(function () {
        $('.invisible').toggleClass('invisible');
    });

    $('#colorButton').click(function () {
        $('dd').css('background-color', 'lightblue');
    });
    $('#colorButton').dblclick(function () {
        $('dd').css('background-color', 'white');
    });


    $('#x').click(function () {
        $('#hideMe').hide();
    });

    $('#register').fadeIn(10000);


})()
