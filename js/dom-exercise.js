(function () {
    /**1. When the button with the id of `change-bg-color` is clicked the background of
     the page should turn blue.*/

    // var buttonBlue = document.getElementById('change-bg-color');
    // buttonBlue.addEventListener('click',function () {
    //     buttonBlue.style.backgroundColor = 'blue';
    // })

    // function myFunction() {
    //     document.body.style.backgroundColor = 'blue';
    // }





    /**2. When the button with an id of `append-to-ul` is clicked, append an li with
     the content of `text` to the ul with the id of `append-to-me`.*/

    // var btnA = document.getElementById('append-to-ul');
    // btnA.addEventListener('click',function () {
    //     var append = document.getElementById('append-to-me');
    //     let li = document.createElement("li");
    //     li += 'text';
    //     console.log(li)
    // })

    // function okFunction(){
    //     var ul = document.getElementById('append-to-me');
    //     var li = document.createElement('li');
    //     li.appendChild(document.createTextNode('text'))
    //     ul.appendChild(li);
    // }
    //
    //
    //
    // /**3. Two seconds after the page loads, the heading with the id of `message` should
    //  change it's text to "Goodbye, World!". Completed*/
    //
    // // setInterval(function () {
    // //     var alert = document.getElementById('message');
    // //     alert.innerText == 'Goodbye, World!';
    // // },2000)
    //
    // setTimeout(function () {
    //     document.getElementById('message').innerHTML = 'Goodbye, World!'
    // },2000)
    //
    //
    //
    //
    // /**4. When a list item inside of the ul with the id of `hl-toggle` is first
    //  clicked, the background of the li that was clicked should change to
    //  yellow. When a list item that has a yellow background is clicked, the
    //  background should change back to the original background.*/
    //
    // Array.from(document.getElementById('hl-toggle').children).forEach(function (elem) {
    //     elem.addEventListener('click', function (e) {
    //         if (e.target.style.backgroundColor === 'yellow') {
    //             return e.target.style.backgroundColor = 'white'
    //         } else {
    //             return e.target.style.backgroundColor = 'yellow'
    //         }
    //
    //     })
    // })


    /**5. When the button with the id of `upcase-name` is clicked, the element with the
     id of `output` should display the text "Your name uppercased is: " + the
     value of the `input` element with the id of `input` transformed to uppercase.*/






    var button1 = document.getElementById('upcase-name');
    button1.addEventListener('click',function () {
        var mainparagraph = document.getElementById('output');
        var entry = document.getElementById('input');
        UpperCasedEntry = entry.value.toUpperCase();
        mainparagraph.innerHTML = 'Your name uppercased is: ' + UpperCasedEntry;
    })




    /**6. Whenever a list item inside of the ul with the id of `font-grow` is _double_
     clicked, the font size of the list item that was clicked should double.*/

    function growFunction() {
        document.getElementById('font-grow').style.fontSize = 'xx-large';
    }


})()