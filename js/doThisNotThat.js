(function () {
"use strict";

const foo = {name: 'tom', age: 30, hungry: false};
const bar = {name: 'dick', age: 40, hungry:false};
const baz = {name: 'harry', age: 50, hungry:true};

//bad code
    console.log(foo);
    console.log(bar);
    console.log(baz);

    //good code
    //Computed Property Names
    console.log({foo, bar, baz});

    //Added styling inside JS
    console.log('%c My Friends', 'color:orange;'); // %c substitutes css style

    //Variables are all similar - you can display as a table to make it easier to read
    console.table({foo, baz, bar});

    console.time('looper');

    let i = 0;
    while (i < 1000000) {i++;}

console.timeEnd('looper');


    const deleteMe = () => console.trace('bye bye database');

    deleteMe();
    deleteMe();

    const turtle = {
        name: 'Bob',
        legs: 4,
        shell: true,
        type: 'amphibious',
        meal: 10,
        diet: 'berries'
    };

    // function feed(animal) {
    //     return `Feed ${animal.name} ${animal.meal} ${animal.diet} a day.`;
    // };

    function feed(animal) {
        const {name, meal, diet} = animal;
        return `Feed ${name} ${meal} ${diet} a day`;
    }




})()